class PlacesController < ApplicationController

  def index
    @places = Place.all
    @place = Place.new
    render "places/index"
  end

  def create
    @place = Place.new(place_params)
    @place.save

    redirect_to places_path
  end

  def edit
    @place = Place.find(params[:id])
  end


  def show
    @place = Place.find(params[:id])
    @review = Review.new
    @reviews = Review.where(:place_id => @place.id)

  end

  def update
    @place = Place.find(params[:id])

    if @place.update(place_params)
      redirect_to @place
    else
      render 'edit'
    end
  end

  def destroy
    @place = Place.find(params[:id])
    @place.destroy

    redirect_to places_path
  end


  def get_places
    @places = Place.all
    render :json => @places.to_json(:include => {:reviews => { :include => :user }})
  end


  private
    def place_params
      params.require(:place).permit(:name, :description, :address)
    end

end