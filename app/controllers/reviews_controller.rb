class ReviewsController < ApplicationController


  def create
    @review = Review.new(review_params)

    @review.save


    respond_to do |format|
      format.html {  redirect_to place_path(@review.place_id) }
      format.json { head :no_content }
    end
  end

  def edit
    @review = Review.find(params[:id])
  end


  def show
    if params[:user_id]
      @review = Review.where(:id => params[:id]).where(:user_id => params[:user_id])
    end

    render :json => @review.to_json
  end

  def update
    @review = Review.find(params[:id])

    if @review.update(place_params)
      redirect_to @review
    else
      render 'edit'
    end
  end

  def destroy
    @review = Review.find(params[:id])

    place = @review.place_id

    @review.destroy
    respond_to do |format|
      format.html {  redirect_to place_path(place) }
      format.json { head :no_content }
    end
  end

  def user_reviews
    @user_id = params[:user_id]
    @reviews = Review.where(:user_id => @user_id)
    render :json => @reviews.to_json(:include => :place)
  end


  private
  def review_params
    params.require(:review).permit(:food_eaten, :description, :place_id, :user_id)
  end

end