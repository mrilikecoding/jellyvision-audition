class UsersController < ApplicationController
  def get_users
    @users = User.all
    render :json => @users.to_json(:include => {:reviews => {:include => :place}})
  end

end