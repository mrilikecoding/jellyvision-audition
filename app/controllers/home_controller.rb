class HomeController < ApplicationController
  def index
    render "home/index"
  end

  def dashboard
    @users = User.all
    @places = Place.all
    @reviews = Review.all

    render "home/dashboard"
  end

end