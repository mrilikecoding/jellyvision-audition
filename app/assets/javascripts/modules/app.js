/**
 Script for CupcakeCrawl

 @class app
 @namespace CupcakeCrawl
 @type {Object}
 **/
var CupcakeCrawl = CupcakeCrawl  || {};

CupcakeCrawl.app = (function($, document, window, undefined) {
    "use strict";

    var reviews;


    /* Public Methods _________________________________________________________________ */

    /**
     The initialization of the page and plugins used in the page.

     @method init

     @return {Null} No return value
     **/
    function init() {

        var App = {}
        window.App = App;

        var globalRefresh = function(){
            var index = new App.Index();
            $('#app').empty();
            $('#app').append(index.render().el)
        }


        var template = function(name){
            return Mustache.compile($('#'+ name + '-template').html());
        }

        App.Review = Backbone.Model.extend({
            defaults: {
                food_eaten : null,
                description: null,
                user_id: window.USER_ID,
                place_id: null
            },
            url: function() {
                return this.id ?  'api/user_reviews/' + window.USER_ID + '/' + this.id : '/api/user_reviews/' + window.USER_ID;
            }
        });


        App.Reviews = Backbone.Collection.extend({
            model: App.Review,
            url: 'api/user_reviews/' + window.USER_ID
        });


        App.Place = Backbone.Model.extend({});

        App.Places = Backbone.Collection.extend({
            model: App.Place,
            url: 'api/places'
        });


        App.Index = Backbone.View.extend({
            template: template('index'),
            initialize: function() {

                this.places = new App.Places();
                this.places.fetch();
                this.reviews = new App.Reviews();
                this.reviews.on('all', this.render, this);
                this.reviews.fetch();

            },
            events: {
                'on renderForm': 'renderForm'
            },
            render: function(){
                this.$el.html(this.template(this));
                var reviewsView = new App.Index.Reviews({collection: this.reviews});
                var placesView = new App.Index.Places({collection: this.places, reviews: this.reviews});
                this.$('.places').append(placesView.render(this.reviews).el);
                this.$('.reviews').append(reviewsView.render().el);
                return this;
            },

            count: function(){
                return this.reviews.length;
            }

        });

        App.Index.Places = Backbone.View.extend({
            render: function(reviews){
                this.reviews = reviews;
                this.collection.each(function(place){
                    var view = new App.Index.Place({model: place});
                    var form = new App.Index.Form({collection: this.reviews});
                    var place_id = place.get('id');
                    this.$el.append(view.render(place_id).el);
                    this.$el.append(form.render(place_id).el);
                }, this);
                return this;
            }
        });

        App.Index.Place = Backbone.View.extend({
            template: template('index-place'),
            className: 'place',
            render: function(place_id){
                this.$el.html(this.template(this)).attr('data-place', place_id);
                return this;
            },
            description: function(){
                return this.model.get('description');
            },
            name: function(){
                return this.model.get('name');
            },
            reviewCount: function(){
                return this.model.get('reviews').length
            },

            address: function(){
                return this.model.get('address')
            },
            currentUserReviewed: function(){
                var reviews = this.model.get('reviews')[0];
                var reviewed = 'No!'
                if (reviews != undefined){
                    if (reviews.user_id == window.USER_ID){
                        reviewed = 'Yes!';

                        this.$el.addClass('reviewed');
                    }
                }
                return reviewed;
            }
        });

        App.Index.Reviews = Backbone.View.extend({
            render: function(){
               this.collection.each(function(review){
                   var view = new App.Index.Review({model: review});
                   this.$el.append(view.render().el);
               }, this);
               return this;
           }
        });

        App.Index.Review = Backbone.View.extend({
            template: template('index-review'),
            events: {
                'click .delete': 'delete'
            },
            initialize: function(){
                var place = this.model.get('place');

                if(place !== undefined){

                    this.place_name = place.name;
                }

            },

            render: function(){
                this.$el.html(this.template(this));
                return this;
            },

            place_name: function(){
                return this.place_name;
            },

            review_time: function(){
                return this.model.get('created_at');
            },

            food_eaten: function(){
                return this.model.get('food_eaten');
            },
            description: function(){
                return this.model.get('description');
            },
            delete: function(){
                this.model.destroy();
                globalRefresh();
            }
        });


        App.Index.Form = Backbone.View.extend({
            tagName: 'form',
            className: 'form',
            template: template('index-form'),
            events: {
                'submit': 'submit'
            },
            render: function(place_id){
                this.place_id = place_id;
                this.$el.html(this.template(this)).attr('data-place', place_id);
                return this;
            },
            submit: function(e){
                e.preventDefault();
                this.collection.create({
                    place_id: this.place_id,
                    food_eaten: this. $('input#food_eaten').val(),
                    description: this.$('input#description').val()
                });
                globalRefresh();

            }

        });


        App.Router = Backbone.Router.extend({
            initialize: function(options) {
                this.el = options.el;
            },

            routes: {
                '': 'index'
            },

            index: function(){
                var index = new App.Index();
                this.el.empty();
                setTimeout(this.el.append(index.render().el), 200);

            }
        });


        App.boot = function(container){
            container = $(container);
            var router = new App.Router({el: container});
            Backbone.history.start();
        }
    }

    /* Private Methods ________________________________________________________________ */


    return {
        init: init
    };

}(jQuery, document, window, undefined));


jQuery(function() {
    "use strict";
    CupcakeCrawl.app.init();
});
