/**
 Script for CupcakeCrawl

 @class home
 @namespace CupcakeCrawl
 @type {Object}
 **/
var CupcakeCrawl = CupcakeCrawl  || {};

CupcakeCrawl.home = (function($, document, window, undefined) {
    "use strict";



    /* Public Methods _________________________________________________________________ */

    /**
     The initialization of the page and plugins used in the page.

     @method init

     @return {Null} No return value
     **/
    function init() {
        if(window.USER_ID !== 'nil'){

            App.boot($('#app'));
        }

    }



    return {
        init: init
    };

}(jQuery, document, window, undefined));


jQuery(function() {
    "use strict";
    CupcakeCrawl.home.init();
});
