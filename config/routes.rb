Rails.application.routes.draw do

  devise_for :users, path_names: {sign_in: "login", sign_out: "logout"}
  root 'home#index'

  get 'dashboard' => 'home#dashboard'

  resources :places
  resources :reviews

  scope 'api' do
    get     'get_places'                   => 'places#get_places'
    get     'get_users'                    => 'users#get_users'

    get     'user_reviews/:user_id'        => 'reviews#user_reviews'
    get     'places/'                      => 'places#get_places'

    post    'user_reviews/:user_id'        => 'reviews#create'
    get     'user_reviews/:user_id/:id'    => 'reviews#show'
    delete  'user_reviews/:user_id/:id'    => 'reviews#destroy'
  end

end
