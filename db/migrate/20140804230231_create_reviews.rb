class CreateReviews < ActiveRecord::Migration
  def change
    create_table :reviews do |t|
      t.string :food_eaten
      t.text :description
      t.integer :user_id
      t.integer :place_id
      t.timestamps
    end
  end
end
