class CreatePlaces < ActiveRecord::Migration
  def change
    create_table :places do |t|
      t.string :address
      t.string :lat
      t.string :long
      t.text :description
    end
  end
end
