class AddNameToPlaces < ActiveRecord::Migration
  def change
    change_table :places do |t|
      t.string :name
      t.string :picture_url
    end
  end
end
